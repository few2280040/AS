import * as playwright from 'playwright-chromium'
import path from 'path'
import {promises as fs} from 'fs'
import os from 'os'

const context = await playwright.chromium.launchPersistentContext(await fs.mkdtemp(path.join(os.tmpdir(), 'pal')), {channel:'chrome', args:['--disable-blink-features=AutomationControlled', '--start-maximized'], headless:false, viewport:null})//default_args https://github.com/microsoft/playwright/blob/5faf6f9e69c2148e94c81675fb636eb31a02b5e7/src%2Fserver%2Fchromium%2Fchromium.ts#L78
const page = await context.newPage()
const headers={'user-agent':await page.evaluateHandle(() => globalThis.navigator.userAgent).then(_ => _.jsonValue()), cookie:'AP_Login=1; AP_Login_E=1; AP_Force_logout=1; AP_Username=VnhWQnd4eG9GYzh0eCs2QXBYY1FKYk9RQjRNcUJlVTlQZmlGK1UyZmQyYz06OpwsmVc0JDqKHP%2F%2FDaOFP7k%3D'}
const advertiser = await globalThis.fetch('https://timebucks.com//publishers/lib/scripts/api/BuyTasksUsersCampaigns.php', {method:'post', headers, body:new globalThis.URLSearchParams({action:'GetAllCampaigns'})}).then(_ => _.json())
const campaigns = advertiser.Campaigns.concat(advertiser.LazyCampaigns).filter(_ => globalThis.Object.is(_.UserId, '223045308')).map(({Id, Proof, Instructions}) => ({Id, Proof, url:Instructions.match(/\bhttps:\/\/\w+\.\w+\/\w+\b/g).at(0)}))
console.log(campaigns)
for (const campaign of campaigns)
{
    await globalThis.fetch('https://timebucks.com/publishers/lib/scripts/api/BuyTasksUsers.php', {method:'post', headers, body:new globalThis.URLSearchParams({action:'CancelCampaign', CampaignId:campaign.Id})})
    console.log(await globalThis.fetch('https://timebucks.com/publishers/lib/scripts/api/BuyTasksUsers.php', {method:'post', headers, body:new globalThis.URLSearchParams({'g-recaptcha-response':'', CampaignId:campaign.Id, action:'ValidateCaptcha'})}).then(_ => _.json()))
    const page = await context.newPage()
    await page.goto(campaign.url)
    await page.goto(new globalThis.URL(await page.locator('iframe[src^="https://googleads.g.doubleclick.net"]').first().frameLocator(':scope').locator('a').first().getAttribute('href', {timeout:0})).searchParams.get('adurl'))
    campaign.screenshot = new globalThis.Blob([await page.screenshot()])
}
await context.close()
await new globalThis.Promise(_ => globalThis.setTimeout(_, 1000 * 60 * 5))
for (const campaign of campaigns)
{
    console.log(await globalThis.fetch('https://timebucks.com/publishers/lib/scripts/api/BuyTasksUsersSubmission.php', {method:'post', headers, body:new globalThis.URLSearchParams({action:'ValidateTimeLimit', CampaignId:campaign.Id})}).then(_ => _.json()))
    const formData = new globalThis.FormData()
    formData.append('action', 'SubmitUserTask')
    formData.append('CampaignId', campaign.Id)
    formData.append('ProofType', campaign.Proof)
    formData.append('Username', '')
    formData.append('SupportFiles', campaign.screenshot, campaign.Id + '.png')
    console.log(globalThis.fetch('https://timebucks.com/publishers/lib/scripts/api/BuyTasksUsersSubmission.php', {method:'post', headers, body:formData}).url)
}
